<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ReservationForm extends FormBase
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'reservation_form';
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['title']= array(
            '#type' => 'checkboxes',
            '#options' => array('Mr' => $this->t('Mr'), 'Mme' => $this->t('Mme'),
                'Mlle' => $this->t('Mlle') ),
        );

        $form['name'] = array(
            '#type' => 'textfield',
            '#attributes' => array(
                'placeholder' => t('Indicate your name')),
            '#title' => t('Nom'),
            '#required' => TRUE,
        );

        $form['phone_number'] = array(
            '#type' => 'tel',
            '#attributes' => array(
                'placeholder' => t('Indicate your phone number ')
            ),
            '#title' => t('Numéro de téléphone'),
        );

        $form['reservation_date'] = array(
            '#type' => 'date',
            '#title' => t('Date de la réservation'),
        );

        $form['reservation_time'] = array(
            '#type' => 'textfield',
            '#title' => t('Heure de la réservation'),
        );


        $form['number_person'] = array(
            '#type' => 'number',
            '#attributes' => array(
                'placeholder' => t('Indicate the number of persons')
            ),
            '#title' => t('Nombre de personnes'),
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Enregistrer'),
        );

        return $form;
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (strlen($form_state->getValue('phone_number')) < 10) {
            $form_state->setErrorByName('phone_number', $this->t('Le numéro de téléphone est trop court. 
            Veuillez rentrer un nunéro à 10 chiffres.'));
        }

//        if ($form_state->getValue('reservation_date') < \Date('now')) {
//            $form_state->setErrorByName('reservation_date', $this->t('La date de réservation est invalide.
//            Veuillez indiquer une autre date'));
//        }

        if (preg_match("/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/", $form_state->getValue('reservation_time') )) {
            $form_state->setErrorByName('reservation_time', $this->t('L heure de réservation est invalide.  
            Veuillez indiquer une heure au format hh:mm'));
        }
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return RedirectResponse
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $title = $form_state->getValue('title');
        $name = $form_state->getValue('name');
        $number_of_person = $form_state->getValue('number_person');
        $phone_number = $form_state->getValue('phone_number');
        $reservation_date = $form_state->getValue('reservation_date');
        $reservation_time = $form_state->getValue('reservation_time');
        $email_address = 'email_du_restaurant@yahoo.fr';

        $send_mail = new PhpMail();
        $from = 'thiesescale@yahoo.fr';
        $message['headers'] = array(
            'content-type' => 'text/html',
            'MIME-Version' => '1.0',
            'reply-to' => $from,
            'from' => 'Name site<'.$from.'>'
        );
        $message['to'] = $email_address;
        $message['subject'] = "Une nouvelle réservation";
        $message['body'] = 'Bonjour, '.$title.' '. $name. 'a réservé pour '. $number_of_person .'personnes 
            La date de réservation est '. $reservation_date . 'à '.$reservation_time.'Son numéro de téléphone 
            est le '.$phone_number;

        $result = $send_mail->mail($message);

        if ($result !== true) {
            drupal_set_message(t('Il y a eu un problème. Veuillez réessayer.'), 'error');
            return new RedirectResponse(\Drupal::url('<front>'));
        }else{
            drupal_set_message($this->t('Bonjour @title @name, votre réservation a bien été enregistrée pour 
                le @date à @heure', array(
                    '@title' => $form_state->getValue('title'),
                    '@name' => $form_state->getValue('name'),
                    '@date' => $form_state->getValue('reservation_date'),
                    '@heure' => $form_state->getValue('reservation_time'),
                )
            ));
            return new RedirectResponse(\Drupal::url('<front>'));
        }

    }
}